const fs = require('fs');
const path = require('path');

const content = JSON.parse(fs.readFileSync(path.resolve(process.cwd(), process.argv[2])).toString());

var id = {
  current: 1,
  next: function () { return this.current++ } 
};

const conversion = {};

const converted = {
  version: 2,
  state: {

    relations: content.scenario.scenes
      .reduce((a, range) => ({
        ...a,
        ...range
          .reduce((b, scene) => ({
            [id.next()]: {
              kind: 'piece',
              parent: conversion[scene.id] || (conversion[scene.id] = id.next()),
              summary: scene.description
            },
            ...b,
            ...scene.actions
              .reduce((c, action) => (console.log(action.id, content.scenario.links), {
                [conversion[action.id] || (conversion[action.id] = id.next())]: {
                  kind: 'piece',
                  parent: conversion[scene.id] || (conversion[scene.id] = id.next()),
                  summary: action.name,
                  ...(action.operators
                    .filter(op => [ 'check-within', 'check-without' ].includes(op.action) )
                    .length > 0
                    ? { guards: action.operators
                      .filter(op => [ 'check-within', 'check-without' ].includes(op.action) )
                      .map(op => ({
                        type: op.action === 'check-within' ? 'in' : 'out',
                        value: op.value,
                        key: op.context
                      })) }
                    : {}),
                  actions: [
                    ...(content.scenario.links && action.id in content.scenario.links
                      ? [{
                          type: 'link',
                          value: conversion[content.scenario.links[action.id]]
                        }]
                      : []),
                    ...action.operators
                      .filter(op => ![ 'check-within', 'check-without' ].includes(op.action) )
                      .map(op => op.action === 'set-tag'
                        ? {
                          type: 'set',
                          key: op.context,
                          value: op.value }
                        : op.action === 'time-pass'
                          ? {
                            type: 'add',
                            key: 'world:time',
                            value: 3600
                          } 
                          : {
                            type: 'delete',
                            key: op.context
                          })
                  ]
                },
                ...c
              }), {})}), {})}), {}),
    layout: content.scenario.scenes
      .map(range => range
        .map(scene => conversion[scene.id]))
  }
}

fs.writeFileSync(path.resolve(process.cwd(), 'converted.json'), JSON.stringify(converted, null, 2));
