import { fromEvent } from "rxjs";
import { share, takeUntil } from 'rxjs/operators';

export const untilUnload = () => takeUntil(fromEvent(window, 'unload').pipe(share()));