import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public get loggedin(): boolean { return !!this.token }
  
  public get token(): string { return (JSON.parse(localStorage.getItem('auth')) || {}).token }
  public set token(token: string) { localStorage.setItem('auth', JSON.stringify({token})) }

  public logout(): void { localStorage.removeItem('auth'); }
}