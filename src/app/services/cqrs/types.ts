export type ServiceSettings = {
  cache: {
    size: number;
  }
}

export type Command<T> = (state: T) => T;
