import { InjectionToken } from '@angular/core';
import { ServiceSettings } from './types';

export const initialState = new InjectionToken<any>('CQRS-initialState');
export const behaviorConfiguration = new InjectionToken<ServiceSettings>('CQRS-behaviorConfiguration');

export const defaultServiceSettings: ServiceSettings = {
  cache: {
    size: 100
  }
}
