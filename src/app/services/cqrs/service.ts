import { Injectable, Inject, Optional } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { initialState, behaviorConfiguration, defaultServiceSettings } from './consts';
import { ServiceSettings, Command } from './types';
import { first, map, combineLatest, count, distinctUntilChanged, withLatestFrom, startWith, shareReplay, tap } from 'rxjs/operators';
import { Dictionary } from 'lodash';

export class Service<T extends Dictionary<any> = Object> {
  public readonly history$ = new BehaviorSubject<T[]>([this.init || {} as T]);
  public readonly pointer$ = new BehaviorSubject<number>(0);
  public readonly state$ = this.history$
    .pipe(
      combineLatest(this.pointer$),
      map(([history, pointer]) => history[pointer]),
      shareReplay(1));
  
  public apply(cmd: Command<T>, historical = true): void {
    this.history$
      .pipe(
        first(),
        withLatestFrom(this.pointer$),
        map(([history, pointer]) => historical
         ? [cmd(history[pointer]), ...history.slice(pointer)]
         : [cmd(history[pointer]), ...history.slice(Math.min(pointer + 1), history.length - 1)]))
      .subscribe(v => this.history$.next(v));
  }

  public undo(count = 1): void {
    this.move(Math.max(0, count));
  }

  public redo(count = 1): void {
    this.move(-Math.max(0, count));
  }

  constructor(
    @Optional() @Inject(initialState) private readonly init: T,
    @Optional() @Inject(behaviorConfiguration) private readonly settings: ServiceSettings
  ) {
    this.settings = this.settings || defaultServiceSettings;
  }

  private move(delta: number): void {
    if (delta > 0) {
      this.pointer$
        .pipe(
          first(),
          withLatestFrom(this.history$
            .pipe(
              first(),
              map(v => v.length))),
          map(([pointer, count]) => Math.max(Math.min(pointer + delta, count - 1), 0)))
        .subscribe(v => this.pointer$.next(v));
    }
  }
}
