import { Injectable } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { Dictionary } from 'lodash';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public on(v: string): Observable<Event> {
    if (!(v in this.cache)) {
      this.cache[v] = fromEvent(document, v).pipe(share());
    }
    return this.cache[v];
  }



  private readonly cache: Dictionary<Observable<Event>> = {};
}