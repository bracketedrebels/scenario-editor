import { Component as NgComponent, ChangeDetectionStrategy, Input, HostBinding } from "@angular/core";

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  selector: 'brs-contextpanel-section-group',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @HostBinding('attr.caption') @Input() public readonly caption: string;
}
