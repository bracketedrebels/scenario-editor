import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
