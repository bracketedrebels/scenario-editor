import { Component as NgComponent, ChangeDetectionStrategy, Input, ContentChild, TemplateRef, Output, EventEmitter } from "@angular/core";
import { Directive } from './directive';

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  selector: 'brs-contextpanel-section',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly caption: string;
  @Input() public readonly collapsed = false;

  @Output() public readonly collapsed$ = new EventEmitter<boolean>();

  @ContentChild(Directive) public readonly dir: Directive;
}
