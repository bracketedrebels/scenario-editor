import { Directive as NgDirective, TemplateRef } from '@angular/core';

@NgDirective({
  selector: '[brs-section-content]'
})
export class Directive {
  constructor( public readonly template: TemplateRef<any> ) { }
}
