import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { ActionModule } from 'src/app/modules/action';
import { Directive } from './directive';

@NgModule({
  imports: [ CommonModule, ActionModule ],
  declarations: [ Component, Directive ],
  exports: [ Component, Directive ]
})
export class Module { }
