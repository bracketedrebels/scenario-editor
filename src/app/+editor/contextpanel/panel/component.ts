import { Component as NgComponent, ChangeDetectionStrategy } from '@angular/core';

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  selector: 'brs-contextpanel',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component { }
