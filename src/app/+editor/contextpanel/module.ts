import { NgModule } from '@angular/core';
import { ContextPanelModule } from './panel';
import { ContextPanelSectionModule } from './section';
import { ContextPanelSectionGroupModule } from './group';

@NgModule({
  imports: [ ContextPanelModule, ContextPanelSectionModule, ContextPanelSectionGroupModule ],
  exports: [ ContextPanelModule, ContextPanelSectionModule, ContextPanelSectionGroupModule ]
})
export class Module {}