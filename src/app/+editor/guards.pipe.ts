import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';
import { GuardDescriptor } from './guard';

@Pipe({
  name: 'guards'
})
export class GuardsPipe implements PipeTransform {
  public transform(v: ScenarioDescriptor, id: number): GuardDescriptor[] {
    return v.relations[id].guards || [];
  }
}
