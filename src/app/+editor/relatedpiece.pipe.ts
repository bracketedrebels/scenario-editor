import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';

@Pipe({
  name: 'relatedpiece'
})
export class RelatedPiecePipe implements PipeTransform {
  public transform(pieceid: number, state: ScenarioDescriptor): ScenarioDescriptor['relations'][0] {
    return state.relations[pieceid];
  }
}