import { Component as NgComponent, ChangeDetectionStrategy, OnDestroy, HostBinding, Inject } from '@angular/core';
import { first, debounceTime, combineLatest, map, filter, withLatestFrom } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';

import { CQRSSerivce, initialState } from '../services/cqrs';
import { recall } from '../helpers/recall';
import { importFile } from '../helpers/importFile';
import { exportFile } from '../helpers/exportFile';
import { remember } from '../helpers/remember';

import { ScenarioDescriptor } from './types';
import { keyState, stateEmpty, UIDs } from './consts';
import { fileDecoder, fileEncoder, acquireInitialState, aquireUIDSeed } from './helpers';
import { deletePiece, deleteScene, deleteAction, updateAction, updateGuard, createAction, createGuard, createPiece, updatePiece, combine, cleanUpPieces, set, clear, cleanUpLayout, unlinkPiece, createScene } from './commands';
import { trigger, transition, style, animate } from '@angular/animations';
import { Omit, entries } from 'lodash';
import { ActionDescriptor } from './action';
import { GuardDescriptor } from './guard';

@NgComponent({
  selector: 'brs-layout-canvas',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: initialState,
    useFactory: acquireInitialState
  }, CQRSSerivce, {
    provide: UIDs,
    deps: [CQRSSerivce],
    useFactory: aquireUIDSeed
  }],
  animations: [ trigger('slide', [
    transition(':enter', [
      style({ transform: 'translateX(20%)', opacity: 0 }),
      animate('.2s ease-in-out', style({ transform: 'translateX(0)', opacity: 1 }))
    ]),
    transition(':leave', [
      style({ transform: 'translateX(0)', opacity: 1 }),
      animate('.2s ease-in-out', style({ transform: 'translateX(20%)', opacity: 0 }))
    ])
  ])]
})
export class Component implements OnDestroy {
  public readonly state = this.cqrs.state$;

  public open(): void {
    importFile(fileDecoder)
      .pipe(
        first())
      .subscribe(v => this.cqrs
        .apply(
          combine(
            set(v.state),
            cleanUpPieces(),
            cleanUpLayout())));
  }

  public unpiece(id: number): void { this.cqrs
    .apply(
      combine(
        deletePiece(id),
        cleanUpLayout()));
  }

  public unaction(target: ActionDescriptor, pieceid: number): void { this.cqrs
    .apply(
      deleteAction(pieceid, target));
  }

  public clear(): void { this.cqrs
    .apply(
      clear());
  }

  public commitAction(pieceid: number, action: ActionDescriptor, patch: Partial<ActionDescriptor>): void {
    this.cqrs.state$
      .pipe(
        first(),
        map(v => v.relations[pieceid].actions
          .find(v => v === action)),
        filter(v => JSON.stringify(v, null, 0) !== JSON.stringify({...v, ...patch}, null, 0)))
      .subscribe(() => this.cqrs
        .apply(
          combine(
            updateAction(pieceid, action, patch),
            cleanUpPieces())));
  }

  public editPiece(pieceid: number, patch: Omit<Partial<ScenarioDescriptor['relations'][0]>, 'kind'>): void {
    this.cqrs
      .apply(
        updatePiece(pieceid, patch))
  }

  public commitGuard(pieceid: number, guard: GuardDescriptor, patch: Partial<GuardDescriptor>): void { this.cqrs.state$
    .pipe(
      first(),
      map(v => v.relations[pieceid].guards
        .find(v => v === guard)),
      filter(v => JSON.stringify(v, null, 0) !== JSON.stringify({...v, ...patch}, null, 0)))
    .subscribe(() => this.cqrs
      .apply(
        updateGuard(pieceid, guard, patch)));
  }

  public addaction(pieceid: number, action: ActionDescriptor): void {
    if (action.type) {
      this.cqrs
        .apply(
          createAction(pieceid, action));
    }
  }

  public addguard(pieceid: number, guard: GuardDescriptor): void {
    if (guard.type && guard.key || guard.type && guard.value !== undefined) {
      this.cqrs
        .apply(
          createGuard(pieceid, guard));
    }
  }

  public addpiece(pieceid: number): void {
    this.cqrs.state$
      .pipe(
        first(),
        map(state => state.relations[pieceid].parent),
        withLatestFrom(this.uids))
      .subscribe(([sceneid, pieceid]) => this.cqrs
        .apply(
          createPiece(pieceid, sceneid)));
  }

  public editSummary(pieceid: number, summary: string): void {
    this.cqrs
      .apply(
        updatePiece(pieceid, { summary }));
  }

  public unlink(pieceid: number) {
    this.cqrs
      .apply(
        unlinkPiece(pieceid));
  }

  public undo(): void { this.cqrs.undo() }
  public redo(): void { this.cqrs.redo() }



  public remove(pieceid: number): void {
    this.cqrs.state$
      .pipe(
        first(),
        map(v => v.relations[pieceid].parent))
      .subscribe(id => this.cqrs
        .apply(
          combine(
            deleteScene(id),
            cleanUpLayout())));
  }

  public clonescene(pieceid: number) {
    this.cqrs.state$
      .pipe(
        first(),
        map(state => {
          const sceneid = state.relations[pieceid].parent;
          const col = state.layout.findIndex(v => v.includes(sceneid));
          const row = state.layout[col].indexOf(sceneid) + 1;

          return [
            entries(state.relations)
              .filter(([, piece]) => piece.parent == sceneid)
              .map(([, piece]) => piece),
            col,
            row
          ] as [ScenarioDescriptor['relations'][0][], number, number]
        }),
        withLatestFrom(this.uids))
      .subscribe( ([[pieces, col, row], newid]) => this.cqrs
        .apply(
          combine(
            createScene(newid, col, row),
            ...pieces
              .map((piece, i) => createPiece(newid + i + 1, newid, piece)))))
  }

  public clonepiece(pieceid: number) {
    this.cqrs.state$
      .pipe(
        first(),
        withLatestFrom(this.uids))
      .subscribe(([state, id]) => this.cqrs
        .apply(
          createPiece(id, state.relations[pieceid].parent, state.relations[pieceid])));
  }

  public download(): void {
    exportFile(recall<ScenarioDescriptor>(keyState, stateEmpty), 'scenario.json', fileEncoder);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  constructor(
    public readonly cqrs: CQRSSerivce<ScenarioDescriptor>,
    @Inject(UIDs) public readonly uids: Observable<number>
  ) {
    this.subscription = this.cqrs.state$
        .pipe(debounceTime(1000)) // prevent updating local storage too often. Long live hardware.
        .subscribe(v => remember(v, keyState))
      .add(this.cqrs.history$
        .pipe(
          combineLatest(this.cqrs.pointer$),
          map(([history, pointer]) => ({
            undoable: pointer < history.length - 1,
            redoable: pointer > 0,
            clearable: history[pointer] && history[pointer] !== stateEmpty
          })),
          map(dict => Object
            .keys(dict)
            .filter(key => dict[key])
            .join(' ')))
        .subscribe(v => this.classes = v));
  }



  @HostBinding('class') private classes: string;
  private readonly subscription: Subscription;
}
