import { Component as NgComponent, Input, ChangeDetectionStrategy } from '@angular/core';

@NgComponent({
  selector: 'brs-guard-type',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    'attr.iconify': ''
  }
})
export class Component {}
