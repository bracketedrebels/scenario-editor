import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { MultivalueInputModule } from 'src/app/modules/multivalue';
import { NoLineBreaksPipeModule } from 'src/app/modules/pipes/nolinebreaks';
import { StartWithPipeModule } from 'src/app/modules/pipes/startwith';
import { NumericPipeModule } from 'src/app/modules/pipes/numeric';
import { InputModule } from 'src/app/modules/input';

@NgModule({
  imports: [
    CommonModule,
    
    NoLineBreaksPipeModule,
    StartWithPipeModule,
    NumericPipeModule,

    MultivalueInputModule,
    InputModule ],

  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
