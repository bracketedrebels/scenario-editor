import { Component as NgComponent, Input, Output, EventEmitter } from '@angular/core';
import { Descriptor } from '../types';
import { Omit } from 'lodash';

@NgComponent({
  selector: 'brs-guard-payload',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ]
})
export class Component {
  @Input() public readonly guard: Descriptor;
  @Output() public readonly guard$ = new EventEmitter<Descriptor>();

  public patch(val: Partial<Omit<Descriptor, 'type'>>): void {
    this.guard$.emit({...this.guard, ...val} as Descriptor);
  }
}
