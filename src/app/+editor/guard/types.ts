export type Kind = 'lt' | 'lte' | 'gt' | 'gte' | 'allin' | 'allout' | 'somein' | 'someout' | 'eq' | 'neq';

export type Descriptor = {
    key: string;
    type: Kind;
  } & ({
    type: 'eq' | 'neq';
    value: number | string | boolean;
  } | {
    type: 'lt' | 'lte' | 'gt' | 'gte';
    value: number;
  } | {
    type: 'allin' | 'allout' | 'somein' | 'someout';
    value: (number | string | boolean)[];
  });