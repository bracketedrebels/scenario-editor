import { Directive as NgDirective, Input, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { Component } from './component';
import { omit } from 'lodash';
import { first, map } from 'rxjs/operators';

@NgDirective({
  selector: '[gotoanchor]'
})
export class Directive implements OnInit, OnDestroy {
  @Input() public readonly gotoanchor: string;

  constructor(
    private readonly ref: ElementRef,
    private readonly host: Component
  ) { }

  public ngOnInit(): void {
    this.host.scenes$
      .pipe(
        first(),
        map(v => ({...v, [this.gotoanchor]: this.ref.nativeElement})))
      .subscribe(v => this.host.scenes$.next(v));
  }
  
  public ngOnDestroy(): void {
    this.host.scenes$
      .pipe(
        first(),
        map(v => omit(v, this.gotoanchor)))
      .subscribe(v => this.host.scenes$.next(v));
  }

}