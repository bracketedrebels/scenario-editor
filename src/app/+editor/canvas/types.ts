import { GuardDescriptor } from '../guard';
import { ActionDescriptor } from '../action';

export type SceneDescriptor = {
  id: number;
  pieces: {
    id: number;
    summary: string;
    guards: GuardDescriptor[];
    actions: ActionDescriptor[];
  }[];
}