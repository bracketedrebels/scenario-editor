import { Component as NgComponent, ChangeDetectionStrategy, EventEmitter, Output, ElementRef } from '@angular/core';

@NgComponent({
  selector: 'brs-gap',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Output() public readonly activated$ = new EventEmitter<void>();
}
