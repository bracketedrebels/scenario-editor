import { Box } from '../../../types';

export type PassageDescriptor = {
  calc: () => Box;
  adjacents: {
    top: string[];
    right: string[];
    bottom: string[];
    left: string[];
  }
}