import { Component as NgComponent, Input, ChangeDetectionStrategy, Output, EventEmitter, ElementRef } from '@angular/core';
import { fromEvent, merge } from 'rxjs';
import { takeUntil, first, filter } from 'rxjs/operators';
import { length } from 'src/app/helpers/vector';

@NgComponent({
  selector: 'brs-scene',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly id: number;

  @Output() public readonly dragstart = new EventEmitter<[HTMLElement, [number, number]]>();

  public ondown(e: MouseEvent): void {
    const point = [e.x, e.y] as [number, number];
    fromEvent(document, 'pointermove')
      .pipe(
        filter((v: MouseEvent) => length(point, [v.x, v.y]) >= 10),
        first(),
        takeUntil(
          merge(
            fromEvent(document, 'pointerup'),
            fromEvent(document, 'pointercancel'))))
      .subscribe(() => this.dragstart.emit([this.eref.nativeElement, point]));
  }

  constructor( private readonly eref: ElementRef ) { }
}
