import { Component as NgComponent, ChangeDetectionStrategy, Input } from '@angular/core';

@NgComponent({
  selector: 'brs-scene-handle',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly id: number;
}
