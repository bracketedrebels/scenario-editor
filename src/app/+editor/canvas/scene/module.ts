import { NgModule } from '@angular/core';
import { Component } from './component';
import { SceneHandleModule } from './handle';


@NgModule({
  imports: [SceneHandleModule],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
