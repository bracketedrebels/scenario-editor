import { Component as NgComponent, Input, AfterViewInit, ViewChild, ElementRef, OnDestroy, ChangeDetectionStrategy, EventEmitter, Output, AfterViewChecked, HostBinding, OnChanges, SimpleChanges, Inject } from '@angular/core';
import ScrollBooster from 'scrollbooster';
import { createPanner } from './helpers';
import { SceneDescriptor } from './types';
import { ScenarioDescriptor } from '../types';
import { fromEvent, merge, asyncScheduler, Observable, identity, BehaviorSubject } from 'rxjs';
import { first, takeUntil, map, subscribeOn, filter, withLatestFrom } from 'rxjs/operators';
import { subtract, length } from 'src/app/helpers/vector';
import { UIDs } from '../consts';
import { flatten, entries, Dictionary } from 'lodash';
import { combine, createRange, createScene, createPiece, relocateScene, updatePiece, cleanUpLayout } from '../commands';

@NgComponent({
  selector: 'brs-canvas',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component implements AfterViewInit, OnDestroy, OnChanges {
  @Input() public readonly state: ScenarioDescriptor;
  @Input() public readonly focused: number | null;
  @Input() public readonly dragging: number | null;
  @Input() public readonly offset: [number, number];

  @Output() public readonly dragging$ = new EventEmitter<number | null>();
  @Output() public readonly focused$ = new EventEmitter<number | null>();
  @Output() public readonly state$ = new EventEmitter<(ScenarioDescriptor) => ScenarioDescriptor>();
  @Output() public readonly offset$ = new EventEmitter<[number, number]>();

  @HostBinding('class.scenedragging') public dragscene = false;
  @HostBinding('class.piecedragging') public dragpiece = false;

  public readonly scenes$ = new BehaviorSubject<Dictionary<HTMLElement>>({});

  public newcol(i: number): void {
    this.uids
      .pipe(
        first(),
        map(v => [v, v + 1] as [number, number]))
      .subscribe(([sceneid, pieceid]) => this.state$
        .emit(
          combine(
            createRange(i),
            createScene(sceneid, i, 0),
            createPiece(pieceid, sceneid))));
  }

  public newscene(i: number, j: number): void {
    this.uids
      .pipe(
        first(),
        map(v => [v, v + 1] as [number, number]))
      .subscribe(([sceneid, pieceid]) => this.state$
        .emit(
          combine(
            createScene(sceneid, i, j),
            createPiece(pieceid, sceneid))));
  }

  public movescene(id: number, i: number, j?: number): void {
    this.state$
      .emit(j !== undefined
        ? combine(
          relocateScene(id, i, j),
          cleanUpLayout())
        : combine(
          createRange(i),
          relocateScene(id, i, 0),
          cleanUpLayout()));
  }

  public isolatePiece(pieceid: number, i: number, j?: number): void {
    const piece = this.state.relations[pieceid];
    const last = entries(this.state.relations).filter(([, v]) => v.parent === piece.parent).length === 1;
    if (last) {
      this.movescene(piece.parent, i, j);
    } else {
      this.uids
        .pipe(first())
        .subscribe(sceneid => this.state$
          .emit(
            combine(
              j === undefined ? createRange(i) : identity,
              createScene(sceneid, i, j),
              updatePiece(pieceid, { parent: sceneid }))));
    }
  }

  public movepiece(pieceid: number, sceneid: number): void {
    this.state$.emit(
      combine(
        updatePiece(pieceid, { parent: sceneid }),
        cleanUpLayout()))
  }

  public movestart(id: number, [el, startpoint]: [HTMLElement, [number, number]]): void {
    fromEvent(this.ref.nativeElement, 'pointermove', { capture: true })
      .pipe(
        map((e: MouseEvent) => subtract([e.x, e.y], startpoint)),
        subscribeOn(asyncScheduler),
        takeUntil(merge(
          fromEvent(document, 'pointerup', { once: true, passive: true }),
          fromEvent(document, 'pointercancel', { once: true, passive: true }))))
      .subscribe({
        next: ([x, y]) => el.style.transform = `translate(${x}px, ${y}px)`,
        complete: () => { this.dragging$.emit(null); el.style.transform = null; }
      });

    this.dragging$.emit(id);
  }

  public goto(sceneorpiece: number): void {
    this.scenes$
      .pipe(
        first(),
        map(scenes => sceneorpiece in scenes
          ? scenes[sceneorpiece]
          : scenes[this.state.relations[sceneorpiece] && this.state.relations[sceneorpiece].parent]),
        filter(v => !!v))
      .subscribe(({offsetLeft, offsetTop}) => this.offset$.emit([offsetLeft, offsetTop]));
  }

  public trackRange(i: number): number { return i >= 0 ? 1 : 0 }
  public trackScene(_, v: SceneDescriptor): number { return v.id }
  public trackPiece(_, v: SceneDescriptor['pieces'][0]): number { return v.id }



  public ngOnChanges({ dragging, offset, state, focused }: SimpleChanges): void {
    this.dragscene = !!dragging && !!dragging.currentValue && flatten(this.state.layout).indexOf(dragging.currentValue) >= 0;
    this.dragpiece = !!dragging && !!dragging.currentValue && dragging.currentValue in this.state.relations;
    if (state && this.scroller) {
      this.scroller.updateMetrics();
    }
    if (offset && offset.currentValue && offset.previousValue && length(offset.currentValue, offset.previousValue) > 1) {
      this.ref.nativeElement.scrollLeft = offset.currentValue[0];
      this.ref.nativeElement.scrollTop = offset.currentValue[1];
    }
  }

  public ngAfterViewInit(): void {
    this.scroller = createPanner(this.ref.nativeElement, this.content.nativeElement, v => this.offset$.emit(v));
    this.scroller.updateMetrics();
  }

  public ngOnDestroy(): void {
    this.scroller.destroy();
  }

  constructor(
    private readonly ref: ElementRef,
    @Inject(UIDs) private readonly uids: Observable<number>
  ) { }



  @ViewChild('content') private readonly content: ElementRef;
  private scroller: ScrollBooster;
}
