import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from '../types';
import { SceneDescriptor } from './types';
import { entries } from 'lodash';

@Pipe({
  name: 'scenes'
})
export class ScenesPipe implements PipeTransform {
  public transform(v: ScenarioDescriptor): SceneDescriptor[][] {
    return v.layout
      .map(range => range
        .map(id => ({
          id,
          pieces: entries(v.relations)
            .filter(([, value]) => value.parent === id && value.kind === 'piece')
            .map(([key, value]) => ({
              id: +key,
              summary: value.summary,
              actions: value.actions || [],
              guards: value.guards || []
            }))
        })))
  }
}