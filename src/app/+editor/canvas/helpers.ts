import ScrollBooster from 'scrollbooster';

export const createPanner = (viewport: HTMLElement, content: HTMLElement, cb: (v: [number, number]) => void) => new ScrollBooster({
  viewport: viewport,
  content: content,
  emulateScroll: true,
  textSelection: true,
  onUpdate: ({position: {x, y}}) => cb([x, y]),
  shouldScroll: (_, event: MouseEvent | TouchEvent) => event
    .composedPath()
    .findIndex(v => v instanceof Element && v.hasAttribute('notahandle')) < 0
});
