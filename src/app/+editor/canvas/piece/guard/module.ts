import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { NamespacePipe } from './namespace.pipe';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Component, NamespacePipe ],
  exports: [ Component ]
})
export class Module {}
