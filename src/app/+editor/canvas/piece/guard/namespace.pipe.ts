import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'namespace'
})
export class NamespacePipe implements PipeTransform {
  public transform(v: string): string {
    return v.split(':')[0];
  }
}
