import { Component as NgComponent, ChangeDetectionStrategy, Input } from '@angular/core';
import { GuardDescriptor } from '../../../guard';

@NgComponent({
  selector: 'brs-guard',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly conf: GuardDescriptor;
}
