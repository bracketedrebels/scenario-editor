import { Component as NgComponent, ChangeDetectionStrategy, Input } from '@angular/core';
import { ActionDescriptor } from '../../../action';

@NgComponent({
  selector: 'brs-action',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly conf: ActionDescriptor;
}
