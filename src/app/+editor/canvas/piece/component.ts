import { Component as NgComponent, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges, HostBinding, ElementRef, HostListener } from '@angular/core';
import { fromEvent, merge } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';
import { length } from 'src/app/helpers/vector';
import { GuardDescriptor } from '../../guard';
import { ActionDescriptor } from '../../action';

@NgComponent({
  selector: 'brs-scene-piece',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component implements OnChanges {
  @Input() public readonly id: string;
  @Input() public readonly guards: GuardDescriptor[] = [];
  @Input() public readonly actions: ActionDescriptor[] = [];
  @Input() public readonly summary: string;

  @Output() public readonly dragstart = new EventEmitter<[HTMLElement, [number, number]]>();

  public ngOnChanges(): void {
    this.modifier = this.actions.length > 0;
  }

  @HostListener('pointerdown', ['$event'])
  public ondown(e: PointerEvent): void {
    const point = [e.x, e.y] as [number, number];
    fromEvent(document, 'pointermove')
      .pipe(
        filter((v: MouseEvent) => length(point, [v.x, v.y]) >= 10),
        first(),
        takeUntil(
          merge(
            fromEvent(document, 'pointerup'),
            fromEvent(document, 'pointercancel'))))
      .subscribe(() => this.dragstart.emit([this.eref.nativeElement, point]));
  }

  constructor( private readonly eref: ElementRef ) { }



  @HostBinding('class.modifier') private modifier = false;
}
