import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { InputModule } from 'src/app/modules/input';
import { PieceActionModule } from './action';
import { PieceGuardModule } from './guard';

@NgModule({
  imports: [CommonModule, InputModule, PieceActionModule, PieceGuardModule],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
