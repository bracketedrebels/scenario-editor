import { PipeTransform, Pipe } from '@angular/core';
import { SceneDescriptor } from './types';

@Pipe({
  name: 'infocus'
})
export class InFocusPipe implements PipeTransform {
  public transform(scene: SceneDescriptor, piece: number): boolean {
    return scene.pieces.filter(v => v.id === piece).length > 0;
  }
}