import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { SceneModule } from './scene';
import { ScenesPipe } from './scenes.pipe';
import { ScenePieceModule } from './piece';
import { GapModule } from './gap';
import { InFocusPipe } from './infocus.pipe';
import { Directive } from './anchor.directive';

@NgModule({
  imports: [
    CommonModule,
    SceneModule,
    ScenePieceModule,
    GapModule ],
  declarations: [ Component, ScenesPipe, InFocusPipe, Directive ],
  exports: [ Component ]
})
export class Module { }
