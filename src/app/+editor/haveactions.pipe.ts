import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';

@Pipe({
  name: 'haveactions'
})
export class HaveActionsPipe implements PipeTransform {
  public transform(state: ScenarioDescriptor, pieceid: number): boolean {
    return !!state.relations[pieceid].actions && state.relations[pieceid].actions.length > 0;
  }
}