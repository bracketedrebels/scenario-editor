import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';
import { ActionDescriptor } from './action';

@Pipe({
  name: 'actions'
})
export class ActionsPipe implements PipeTransform {
  public transform(v: ScenarioDescriptor, id: number): ActionDescriptor[] {
    return v.relations[id].actions;
  }
}
