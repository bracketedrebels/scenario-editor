import { Pipe, PipeTransform } from '@angular/core';
import { ScenarioDescriptor } from './types';

@Pipe({
  name: 'linked'
})
export class LinkedPipe implements PipeTransform {
  public transform(piece: ScenarioDescriptor['relations'][0]): number {
    const link = (piece.actions || []).filter(v => v.type === 'link')[0];
    return link && link.type === 'link' && link.value;
  }
}