import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { ScenarioDescriptor } from './types';

export const keyState = 'state';
export const stateEmpty: ScenarioDescriptor = { layout: [[]], relations: {} };
export const UIDs = new InjectionToken<Observable<string>>('sceneUIDs');
