import { ScenarioDescriptor } from './types';
import { omit, entries, Omit, flatten } from 'lodash';
import { insertBefore } from '../helpers/insertBefore';
import { substitute } from '../helpers/substitute';
import { stateEmpty } from './consts';
import { ActionDescriptor } from './action';
import { GuardDescriptor } from './guard';

export const combine = (...commands: ((state: ScenarioDescriptor) => ScenarioDescriptor)[]) => (state: ScenarioDescriptor) =>
  commands.reduce((acc, v) => v(acc), state);

export const cleanUpLayout = () => (state: ScenarioDescriptor) => {
  const pieces = entries(state.relations);
  return {
    ...state,
    layout: state.layout
      .map(range => range
        .filter(sceneid => pieces
          .filter(([, scene]) => scene.parent === sceneid).length > 0))
      .filter(range => range.length > 0),
  }
}

export const cleanUpPieces = () => (state: ScenarioDescriptor) => {
  const scenes = flatten(state.layout);
  return {
    ...state,
    relations: entries(state.relations)
      .filter(([, piece]) => scenes.indexOf(piece.parent) >= 0)
      .reduce((acc, [pieceid, piece]) => ({
        ...acc,
        [pieceid]: {
          ...piece,
          actions: (piece.actions || [])
            .filter(v => v.type === 'link'
              ? scenes.indexOf(v.value) >= 0
              : true )
        }
      }), {})
  } as ScenarioDescriptor
}

export const set = (state: ScenarioDescriptor) => () => state;
export const clear = () => () => stateEmpty;

export const createRange = (index: number) => (state: ScenarioDescriptor) => ({
  ...state,
  layout: insertBefore(state.layout)(index, [])
})

export const createScene = (sceneid: number, col: number, row: number) => (state: ScenarioDescriptor) => ({
  ...state,
  layout: substitute(state.layout)(col, insertBefore(state.layout[col])(row, sceneid))
})

export const relocateScene = (sceneid: number, col: number, row: number) => (state: ScenarioDescriptor) => (console.log(sceneid, col, row), {
  ...state,
  layout: state.layout
    .map((range, i) => i === col
      ? range.indexOf(sceneid) >= 0
        ? insertBefore(range.filter(v => v !== sceneid))(range.indexOf(sceneid) >= row ? row : Math.max(0, row - 1), sceneid)
        : insertBefore(range)(row, sceneid)
      : range.indexOf(sceneid) >= 0
        ? range.filter(v => v !== sceneid)
        : range)
})

export const createPiece = (pieceid: number, sceneid: number, patch: Omit<Partial<ScenarioDescriptor['relations'][0]>, 'kind' | 'parent'> = {}) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      kind: 'piece',
      parent: sceneid,
      summary: '',
      ...omit(patch, ['kind', 'parent'])
    }
  }
})

export const createAction = (pieceid: number, action: ActionDescriptor) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      actions: [
        ...(state.relations[pieceid].actions || []),
        action
      ]
    }
  }
})

export const createGuard = (pieceid: number, guard: GuardDescriptor) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      guards: [
        ...(state.relations[pieceid].guards || []),
        guard
      ]
    }
  }
})

export const deleteAction = (pieceid: number, action: ActionDescriptor) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      actions: state.relations[pieceid].actions.filter(v => v !== action)
    }
  }
})

export const deleteGuard = (pieceid: number, guard: GuardDescriptor) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      guards: state.relations[pieceid].guards.filter(v => v !== guard)
    }
  }
})

export const deletePiece = (pieceid: number) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: omit(state.relations, pieceid)
})

export const deleteScene = (sceneid: number) => (state: ScenarioDescriptor) => ({
  ...state,
  layout: state.layout
    .map(range => range.indexOf(sceneid) >= 0
      ? range.filter(v => v !== sceneid)
      : range)
})

export const updateAction = (pieceid: number, action: ActionDescriptor, patch: Partial<ActionDescriptor>) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      actions: state.relations[pieceid].actions
        .map(v => v === action ? {...v, ...patch} : v)
    }
  }
})

export const updateGuard = (pieceid: number, guard: GuardDescriptor, patch: Partial<GuardDescriptor>) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      guards: state.relations[pieceid].guards
        .map(v => v === guard ? {...v, ...patch} : v)
    }
  }
})

export const updatePiece = (pieceid: number, patch: Omit<Partial<ScenarioDescriptor['relations'][0]>, 'kind'>) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      ...patch
    }
  }
})

export const unlinkPiece = (pieceid: number) => (state: ScenarioDescriptor) => ({
  ...state,
  relations: {
    ...state.relations,
    [pieceid]: {
      ...state.relations[pieceid],
      actions: (state.relations[pieceid].actions || [])
        .filter(v => v.type !== 'link')
    }
  }
})