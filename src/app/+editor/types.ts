import { Dictionary } from 'lodash';
import { GuardDescriptor } from './guard';
import { ActionDescriptor } from './action';

export type ScenarioDescriptor = {
  layout: number[][];
  relations: Dictionary<{
    kind: 'piece';
    parent: number;
    summary: string;
    guards?: GuardDescriptor[];
    actions?: ActionDescriptor[];
  }>;
};

export type ScenarioFileStructure = {
  version: number;
  state: ScenarioDescriptor;
}
