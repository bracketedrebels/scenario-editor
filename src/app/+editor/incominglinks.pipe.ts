import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';
import { entries } from 'lodash';

@Pipe({
  name: 'incominglinks'
})
export class IncomingLinksPipe implements PipeTransform {
  public transform(state: ScenarioDescriptor, sceneid: number): {scene: number; piece: number}[] {
    return entries(state.relations)
      .reduce((acc, [piece, {parent, actions}]) => actions && actions
        .find(v => v.type === 'link' && v.value === sceneid)
          ? [...acc, { scene: parent, piece: Number(piece) }]
          : acc, []);
  }
}
