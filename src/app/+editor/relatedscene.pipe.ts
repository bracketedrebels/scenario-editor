import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';

@Pipe({
  name: 'relatedscene'
})
export class RelatedScenePipe implements PipeTransform {
  public transform(state: ScenarioDescriptor, pieceid: number): number {
    return state.relations[pieceid].parent;
  }
}