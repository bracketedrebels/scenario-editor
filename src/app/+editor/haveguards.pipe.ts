import { PipeTransform, Pipe } from '@angular/core';
import { ScenarioDescriptor } from './types';

@Pipe({
  name: 'haveguards'
})
export class HaveGuardsPipe implements PipeTransform {
  public transform(state: ScenarioDescriptor, pieceid: number): boolean {
    return !!state.relations[pieceid].guards && state.relations[pieceid].guards.length > 0;
  }
}