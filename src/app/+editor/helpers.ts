import { ScenarioDescriptor, ScenarioFileStructure } from './types';
import { Dictionary, entries } from 'lodash';
import { keyState, stateEmpty } from './consts';
import { recall } from '../helpers/recall';
import { flatten } from 'lodash';
import { CQRSSerivce } from '../services/cqrs';
import { map, reduce } from 'rxjs/operators';

export const aquireUIDSeed = (service: CQRSSerivce<ScenarioDescriptor>) => service.state$
  .pipe(
    map(state => flatten(state.layout)
      .reduce((acc, v) => v > acc ? v : acc, Object
      .keys(state.relations)
      .reduce((acc, v) => Number.parseInt(v) > acc ? Number.parseInt(v) : acc, 0)) + 1));

export function acquireInitialState(): any {
  return recall(keyState, stateEmpty);
}

export const fileDecoder: (v: string) => ScenarioFileStructure = (v: string) => {
  const parsed = JSON.parse(v);
  const version = parsed.version || 0;
  return ([ convert0to1, convert1to2, convert2to3 ] as ((v: any) => any)[])
    .slice(version)
    .reduce((acc, converter) => converter(acc), parsed);
}

export const fileEncoder = (scenario: ScenarioDescriptor) => {
  const fileContent: ScenarioFileStructure = {
    version: 2,
    state: scenario
  };
  return JSON.stringify(fileContent, null, 0);
}


const convert0to1 = (scenes: {
    id: string;
    description?: string;
    actions: {
      id: string;
      name?: string;
      operators: {
        action: 'set-tag' | 'remove-tag' | 'check-within' | 'check-without' | 'time-pass';
        value: string;
        context: string;
      }[];
    }[];
  }[][]) => ({ version: 1, scenario: { scenes } });


const convert1to2 = (content: {
  version: number;
  scenario: {
    scenes: {
      id: string;
      description?: string;
      actions: {
        id: string;
        name?: string;
        operators: {
          action: 'set-tag' | 'remove-tag' | 'check-within' | 'check-without' | 'time-pass';
          value: string;
          context: string;
        }[];
      }[];
    }[][],
    links: Dictionary<string>;
  }
}) => {
  var id = {
    current: 1,
    next: function () { return this.current++ } 
  };
  
  const conversion = {};
  
  return {
    version: 2,
    state: {
  
      relations: content.scenario.scenes
        .reduce((a, range) => ({
          ...a,
          ...range
            .reduce((b, scene) => ({
              [id.next()]: {
                kind: 'piece',
                parent: conversion[scene.id] || (conversion[scene.id] = id.next()),
                summary: scene.description
              },
              ...b,
              ...scene.actions
                .reduce((c, action) => ({
                  [conversion[action.id] || (conversion[action.id] = id.next())]: {
                    kind: 'piece',
                    parent: conversion[scene.id] || (conversion[scene.id] = id.next()),
                    summary: action.name,
                    ...(action.operators
                      .filter(op => [ 'check-within', 'check-without' ].includes(op.action) )
                      .length > 0
                      ? { guards: action.operators
                        .filter(op => [ 'check-within', 'check-without' ].includes(op.action) )
                        .map(op => ({
                          type: op.action === 'check-within' ? 'in' : 'out',
                          value: op.value,
                          key: op.context
                        })) }
                      : {}),
                    actions: [
                      ...(content.scenario.links && action.id in content.scenario.links
                        ? [{
                            type: 'link',
                            value: conversion[content.scenario.links[action.id]]
                          }]
                        : []),
                      ...action.operators
                        .filter(op => ![ 'check-within', 'check-without' ].includes(op.action) )
                        .map(op => op.action === 'set-tag'
                          ? {
                            type: 'set',
                            key: op.context,
                            value: op.value }
                          : op.action === 'time-pass'
                            ? {
                              type: 'add',
                              key: 'world:time',
                              value: 3600
                            } 
                            : {
                              type: 'delete',
                              key: op.context
                            })
                    ]
                  },
                  ...c
                }), {})}), {})}), {}),
      layout: content.scenario.scenes
        .map(range => range
          .map(scene => conversion[scene.id]))
    }
  } as ScenarioFileStructure
}

export const convert2to3 = content => ({
  version: 3,
  state: {
    ...content.state,
    relations: entries(content.state.relations)
      .reduce((acc, [key, value]: [string, any]) => ({
        ...acc,
        [key]: {
          ...value,
          guards: (value.guards || [])
            .map(v => v.type === 'in'
              ? ({ ...v, type: 'allin' })
              : v.type === 'out'
                ? ({ ...v, type: 'allout' })
                : v)
        }
      }) as ScenarioDescriptor['relations'], {} as ScenarioDescriptor['relations'])
  }
}) as ScenarioFileStructure
  