import { NgModule } from '@angular/core';
import { SectionModule } from './section';
import { StatusBarModule } from './bar';

@NgModule({
  imports: [ SectionModule, StatusBarModule ],
  exports: [ SectionModule, StatusBarModule ]
})
export class Module { }
