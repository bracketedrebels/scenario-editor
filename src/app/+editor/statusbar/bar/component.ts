import { Component as NgComponent, ChangeDetectionStrategy } from '@angular/core';

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  selector: 'brs-statusbar',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component { }
