import { NgModule } from '@angular/core';
import { Component } from './component';
import { LayoutCanvasModule } from './canvas';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StatusBarModule } from './statusbar';
import { ContextPanelModule } from './contextpanel';
import { ActionsPipe } from './actions.pipe';
import { GuardsPipe } from './guards.pipe';
import { ActionModule } from '../modules/action';
import { InputModule } from '../modules/input';
import { HaveActionsPipe } from './haveactions.pipe';
import { HaveGuardsPipe } from './haveguards.pipe';
import { RelatedScenePipe } from './relatedscene.pipe';
import { RelatedPiecePipe } from './relatedpiece.pipe';
import { ActionsEditorModule } from './editors/actions';
import { GuardsEditorModule } from './editors/guards';
import { LinksEditorModule } from './editors/links';
import { IncomingLinksPipe } from './incominglinks.pipe';
import { LinkedPipe } from './linked.pipe';
import { NoLineBreaksPipeModule } from '../modules/pipes/nolinebreaks';
import { StartWithPipeModule } from '../modules/pipes/startwith';

@NgModule({
  imports: [
    LayoutCanvasModule,
    StatusBarModule,
    ContextPanelModule,
    InputModule,
    ActionModule,
    CommonModule,
    NoLineBreaksPipeModule,
    StartWithPipeModule,
    ActionsEditorModule,
    GuardsEditorModule,
    LinksEditorModule,
    RouterModule.forChild([{
        path: '',
        component: Component,
        children: [{
          path: 'play',
          loadChildren: './+play/module#Module'
        }]
      }])],
  declarations: [Component, ActionsPipe, GuardsPipe, HaveGuardsPipe, HaveActionsPipe, RelatedScenePipe, RelatedPiecePipe, IncomingLinksPipe, LinkedPipe]
})
export class Module { }
