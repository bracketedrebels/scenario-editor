import { PipeTransform, Pipe } from '@angular/core';
import { ActionDescriptor } from '../../action';

@Pipe({
  name: 'invalid'
})
export class InvalidPipe implements PipeTransform {
  public transform(v: ActionDescriptor): boolean {
    switch (v.type) {
      case 'negate':
        return !v.key;
      default:
    }
    return !v || !v.type || !v.key;
  }
}