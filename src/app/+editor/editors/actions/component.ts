import { Component as NgComponent, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';
import { ActionDescriptor } from '../../action';

@NgComponent({
  selector: 'brs-actions-editor',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly actions: ActionDescriptor[];
  @Output() public readonly actions$ = new EventEmitter<ActionDescriptor[]>();
  @Output() public readonly goto$ = new EventEmitter<number>();

  public unaction(target: ActionDescriptor): void {
    if (this.actions && this.actions.length > 0) {
      this.actions$
        .emit(this.actions
          .filter(v => v !== target))
    }
  }

  public patch(source: ActionDescriptor, target: ActionDescriptor): void {
    if (this.actions && this.actions.length > 0) {
      this.actions$
        .emit(this.actions
          .map(v => v === source ? target : v))
    }
  }

  public append(piece: ActionDescriptor): void {
    this.actions$
      .emit([...(this.actions || []), piece]);
  }
}
