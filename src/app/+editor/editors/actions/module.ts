import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { InvalidPipe } from './valid.pipe';
import { ActionTypeModule, ActionPayloadModule } from '../../action';
import { NoLineBreaksPipeModule } from 'src/app/modules/pipes/nolinebreaks';
import { StartWithPipeModule } from 'src/app/modules/pipes/startwith';
import { ActionModule } from 'src/app/modules/action';
import { InputModule } from 'src/app/modules/input';

@NgModule({
  imports: [
    CommonModule,

    ActionModule,
    InputModule,
    ActionTypeModule,
    ActionPayloadModule,

    StartWithPipeModule,
    NoLineBreaksPipeModule ],

  declarations: [ Component, InvalidPipe ],
  exports: [ Component ]
})
export class Module { }