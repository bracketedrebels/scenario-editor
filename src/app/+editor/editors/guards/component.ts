import { Component as NgComponent, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';
import { GuardDescriptor } from '../../guard';

@NgComponent({
  selector: 'brs-guards-editor',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly guards: GuardDescriptor[];
  @Output() public readonly guards$ = new EventEmitter<GuardDescriptor[]>();

  public unguard(target: GuardDescriptor): void {
    if (this.guards && this.guards.length > 0) {
      this.guards$
        .emit(this.guards
          .filter(v => v !== target))
    }
  }

  public patch(source: GuardDescriptor, target: GuardDescriptor): void {
    if (this.guards && this.guards.length > 0) {
      this.guards$
        .emit(this.guards
          .map(v => v === source ? target : v))
    }
  }

  public append(guard: GuardDescriptor): void {
    this.guards$
      .emit([...(this.guards || []), guard]);
  }
}
