import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { ActionModule } from 'src/app/modules/action';
import { InputModule } from 'src/app/modules/input';
import { InvalidPipe } from './valid.pipe';
import { GuardPayloadModule, GuardTypeModule } from '../../guard';
import { StartWithPipeModule } from 'src/app/modules/pipes/startwith';

@NgModule({
  imports: [
    CommonModule,
    
    ActionModule,
    InputModule,
    GuardTypeModule,
    GuardPayloadModule,

    StartWithPipeModule ],

  declarations: [ Component, InvalidPipe ],
  exports: [ Component ]
})
export class Module { }