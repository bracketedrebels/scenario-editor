import { PipeTransform, Pipe } from '@angular/core';
import { GuardDescriptor } from '../../guard';

@Pipe({
  name: 'invalid'
})
export class InvalidPipe implements PipeTransform {
  public transform(v: GuardDescriptor): boolean {
    return !v || !v.type || !v.key;
  }
}