import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { ActionModule } from 'src/app/modules/action';

@NgModule({
  imports: [ CommonModule, ActionModule ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }