import { Component as NgComponent, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';

@NgComponent({
  selector: 'brs-links-editor',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly links: {scene: number; piece: number}[];
  @Output() public readonly unlink$ = new EventEmitter<number>();
  @Output() public readonly goto$ = new EventEmitter<number>();
}
