import { Component as NgComponent, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';
import { Omit } from 'lodash';
import { Descriptor } from '../types';

@NgComponent({
  selector: 'brs-action-payload',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public action: Descriptor; 
  @Output() public readonly action$ = new EventEmitter<Descriptor>();

  public patch(action: Descriptor, patch: Omit<Descriptor, 'key'>): void {
    this.action$.emit({...action, ...patch} as Descriptor);
  }
}
