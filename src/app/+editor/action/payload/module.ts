import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { InputModule } from 'src/app/modules/input';
import { MultivalueInputModule } from 'src/app/modules/multivalue';
import { NoLineBreaksPipeModule } from 'src/app/modules/pipes/nolinebreaks';
import { NumericPipeModule } from 'src/app/modules/pipes/numeric';
import { StartWithPipeModule } from 'src/app/modules/pipes/startwith';

@NgModule({
  imports: [
    CommonModule,
    
    InputModule,
    MultivalueInputModule,
    
    StartWithPipeModule,
    NumericPipeModule,
    NoLineBreaksPipeModule ],

  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
