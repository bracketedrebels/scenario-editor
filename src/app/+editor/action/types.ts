export type Kind = 'set' | 'delete' | 'add' | 'multiply' | 'negate' | 'push' | 'pop' | 'link';

export type Descriptor = {
    key: string;
    type: Kind
  } & ({
    type: 'negate';
  } | {
    type: 'set' | 'delete';
    value: number | string | boolean;
  } | {
    type: 'push' | 'pop';
    value: (number | string | boolean)[];
  } | {
    type: 'add'| 'multiply' | 'link';
    value: number;
    key: void;
  });
