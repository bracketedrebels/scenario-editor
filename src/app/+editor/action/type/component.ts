import { Component as NgComponent, ChangeDetectionStrategy, Input } from '@angular/core';
import { Kind } from '../types';

@NgComponent({
  selector: 'brs-action-type',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    'iconify': ''
  }
})
export class Component {
  @Input() public readonly type: Kind;
}
