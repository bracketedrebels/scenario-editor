import { Component as NgComponent, ChangeDetectionStrategy } from '@angular/core';

@NgComponent({
  selector: 'brs-sidepanel',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component { }