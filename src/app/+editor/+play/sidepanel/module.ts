import { NgModule } from '@angular/core';
import { SidepanelBodyModule } from './panel';
import { SidepanelSectionModule } from './section';

@NgModule({
  imports: [ SidepanelBodyModule, SidepanelSectionModule ],
  exports: [ SidepanelBodyModule, SidepanelSectionModule ]
})
export class Module { }
