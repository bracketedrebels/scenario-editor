import { Component as NgComponent, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { CQRSSerivce } from 'src/app/services/cqrs';
import { ScenarioDescriptor } from '../types';
import { Subscription, BehaviorSubject } from 'rxjs';
import { map, shareReplay, zip, combineLatest } from 'rxjs/operators';
import { trigger, transition, style, animate } from '@angular/animations';
import { x86 as murmur } from 'murmurhash3js';
import { Dictionary, flatten, entries } from 'lodash';
import { composeGuards, composeActions } from './helpers';
import { ActionDescriptor } from '../action';

@NgComponent({
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('animate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('.5s', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('.3s', style({ opacity: 0 }))
      ])
    ])],
  host: {
    '[@animate]': 'true'
  }
})
export class Component implements OnDestroy {
  public readonly state = new BehaviorSubject<Dictionary<any>>({});
  public readonly digest = this.cqrs.state$
    .pipe(
      map( state => String(murmur.hash32(JSON.stringify(state, null, 0))) ),
      shareReplay(1));

  public readonly scenes = this.digest
    .pipe(
      zip(this.cqrs.state$),
      map(([fp, state]) => flatten(state.layout)
        .reduce((acc, sceneid) => ({
          ...acc,
          [sceneid]: entries(state.relations)
            .filter(([, piece]) => piece.parent === sceneid)
            .map(([, piece]) => ({
              ...piece,
              actions: piece.actions || [],
              guards: piece.guards || []
            }))
            .map(piece => ({
              ...piece,
              actions: (piece.actions.length > 0
                ? piece.actions.find(v => v.type === 'link')
                  ? piece.actions.map(v => v.type === 'link' ? { ...v, key: fp } : v)
                  : [...piece.actions, { type: 'link', key: fp, value: piece.parent } ]
                : []) as ActionDescriptor[]
            }))
            .map(piece => ({
              description: piece.summary,
              guard: composeGuards(piece.guards || []),
              ...piece.actions.length > 0
                ? { action: composeActions(piece.actions) }
                : { }
            }))
        }), {})))

  public readonly pieces = this.scenes
    .pipe(
      combineLatest(this.digest, this.state),
      map(([scenes, digest, state]) => scenes[state[digest]]))

  constructor( private readonly cqrs: CQRSSerivce<ScenarioDescriptor> ) { }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }



  private readonly subscription: Subscription = this.digest
    .subscribe(fp => {
      this.state.next({
        [fp]: 5
      })
    })
}