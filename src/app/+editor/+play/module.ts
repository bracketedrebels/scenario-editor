import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Component } from './component';
import { DefaultSceneRenderingModule } from './scene';
import { CommonModule } from '@angular/common';
import { SidepanelModule } from './sidepanel';

@NgModule({
  imports: [
    DefaultSceneRenderingModule,
    SidepanelModule,
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: Component }]) ],
  declarations: [ Component ],
})
export class Module { }
