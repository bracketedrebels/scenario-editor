import { NgModule } from '@angular/core';
import { Component } from './component';
import { PiecesGuard } from './filter';
import { CommonModule } from '@angular/common';
import { DescsPipe } from './descs.pipe';
import { MutatorsPipe } from './mutators.pipe';

@NgModule({
  declarations: [ Component, PiecesGuard, DescsPipe, MutatorsPipe ],
  imports: [CommonModule],
  entryComponents: [ Component ],
  exports: [ Component ]
})
export class Module { }

