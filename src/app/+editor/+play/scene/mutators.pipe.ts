import { PipeTransform, Pipe } from '@angular/core';
import { Piece } from './types';

@Pipe({
  name: 'mutators'
})
export class MutatorsPipe implements PipeTransform {
  public transform(pieces: Piece[]): Piece[] {
    return pieces.filter(v => !!v.action);
  }
}