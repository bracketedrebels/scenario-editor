import { Component as NgComponent, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Piece } from './types';
import { Dictionary } from 'lodash';

@NgComponent({
  selector: 'brs-scene',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class Component {
  @Input() public pieces: Piece[];
  @Input() public state: Dictionary<any>;

  @Output() public readonly state$ = new EventEmitter<Dictionary<any>>();
}
