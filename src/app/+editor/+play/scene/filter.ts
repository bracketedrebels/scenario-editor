import { Pipe, PipeTransform } from "@angular/core";
import { Piece } from './types';
import { Dictionary } from 'lodash';

@Pipe({
    name: 'piecesguard'
})
export class PiecesGuard implements PipeTransform {
    public transform(items: Piece[], state: Dictionary<any>): Piece[] {
        return items.filter(item => item.guard(state));
    }
}