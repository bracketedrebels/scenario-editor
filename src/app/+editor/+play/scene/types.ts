import { Dictionary } from 'lodash';

export type Piece = {
  guard: (state: Dictionary<any>) => boolean;
  description: string;
  action?: (state: Dictionary<any>) => typeof state;
};