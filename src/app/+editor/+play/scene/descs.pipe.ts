import { PipeTransform, Pipe } from '@angular/core';
import { Piece } from './types';

@Pipe({
  name: 'descs'
})
export class DescsPipe implements PipeTransform {
  public transform(pieces: Piece[]): Piece[] {
    return pieces.filter(v => !v.action);
  }
}