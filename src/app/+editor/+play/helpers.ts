import { Dictionary, omit, intersection } from 'lodash';
import { ActionDescriptor } from '../action';
import { GuardDescriptor } from '../guard';
import { Atomic } from './types';

const set = (key: string, value: any) => (state: Dictionary<any>) => Array.isArray(state[key]) ? ({ ...state, [key]: [...state[key], value] }) : ({ ...state, [key]: value });
const del = (key: string, value?: any) => value !== undefined
    ? (state: Dictionary<any>) => ( key in state ? { ...state, [key]: state[key].filter(x => x !== value) }: state)
    : (state: Dictionary<any>) => omit(state, key);
const add = (key: string, value: number) => (state: Dictionary<any>) => ({ ...state, [key]: (state[key] || 0) + value });
const mul = (key: string, value: number) => (state: Dictionary<any>) => ({ ...state, [key]: (state[key] || 0) * value });

const negate = (key: string) => (state: Dictionary<any>) => ({ ...state, [key]: !state[key] });
const link = (key: string, value: number) => (state: Dictionary<any>) => ({ ...state, [key]: value });
const nothing = () => (state: Dictionary<any>) => state;

function pieceTransformer(action: ActionDescriptor): (state: Dictionary<any>) => typeof state {
    switch (action.type) {
        case 'negate': {
            return negate(action.key);
        }
        case 'delete': {
            return del(action.key, action.value);
        }
        case 'set': {
            return set(action.key, action.value);
        }
        case 'add': {
            return add(action.key, action.value);
        }
        case 'multiply': {
            return mul(action.key, action.value);
        }
        case 'link': {
            return link(action.key, action.value);
        }
        default: {
            return nothing();
        }
    }

}

const lt  = (key: string, value: number) => (state: Dictionary<any>) => value < state[key];
const lte = (key: string, value: number) => (state: Dictionary<any>) => value <= state[key];
const gt  = (key: string, value: number) => (state: Dictionary<any>) => value > state[key];
const gte = (key: string, value: number) => (state: Dictionary<any>) => value >= state[key];
const eq = (key: string, value: Atomic ) => (state: Dictionary<any>) => value === state[key];
const neq = (key: string, value: Atomic) => (state: Dictionary<any>) => value !== state[key];

const normalize = <T>(v: T | T[]) => v ? Array.isArray(v) ? v : [v] : []

const allin = (key: string, value: Atomic | Atomic[]) => {
  const normalized = normalize(value);
  return (state: Dictionary<any>) => intersection(normalized, normalize(state[key])).length === normalized.length;
};

const allout = (key: string, value: Atomic | Atomic[]) => {
  const normalized = normalize(value);
  return (state: Dictionary<any>) => intersection(normalized, normalize(state[key])).length === 0;
};
const somein = (key: string, value: Atomic | Atomic[]) => {
  const normalized = normalize(value);
  return (state: Dictionary<any>) => intersection(normalized, normalize(state[key])).length > 0;
};
const someout = (key: string, value: Atomic | Atomic[]) => {
  const normalized = normalize(value);
  return (state: Dictionary<any>) => intersection(normalized, normalize(state[key])).length < normalized.length;
};


function guardTransformer(guard: GuardDescriptor): (state: Dictionary<any>) => boolean {
    switch (guard.type) {
        case 'lt': {
            return lt(guard.key, guard.value);
        }
        case 'lte': {
            return lte(guard.key, guard.value);
        }
        case 'gt': {
            return gt(guard.key, guard.value);
        }
        case 'gte': {
            return gte(guard.key, guard.value);
        }
        case 'allin': {
            return allin(guard.key, guard.value);
        }
        case 'allout': {
            return allout(guard.key, guard.value);
        }
        case 'somein': {
          return allin(guard.key, guard.value);
        }
        case 'someout': {
            return allout(guard.key, guard.value);
        }
        case 'eq': {
            return eq(guard.key, guard.value);
        }
        case 'neq': {
          return neq(guard.key, guard.value);
        }
        default: {
            return () => true;
        }
    }
}

export const composeGuards = (guards: GuardDescriptor[]) => {
    const guardsList = guards
        .reduce((acc, v) => [...acc, guardTransformer(v)], [] as Array<(state: Dictionary<any>) => boolean>)
    return (state: Dictionary<any>) => guardsList.reduce((acc, v) => v(state) && acc, true);
}


export const composeActions = (actions: ActionDescriptor[]) => {
    const mutatorsList = actions
        .reduce((acc, v) => [...acc, pieceTransformer(v)], [] as Array<(state: Dictionary<any>) => Dictionary<any>>);
    return (state: Dictionary<any>) => mutatorsList.reduce((acc, v) => v(acc), state);
}
