export const insertBefore = <T>(list: T[]) => (index: number, target: T) => {
  return index < 0
   ? list
   : [
     ...list.slice(0, index),
     target,
     ...list.slice(index)
   ];
}