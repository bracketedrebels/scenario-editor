import { saveAs } from 'file-saver';

export const exportFile = <T>(data: T, fname: string, encoder: (v: T) => Blob | string = (v: T) => JSON.stringify(v)) => {
  saveAs(new Blob([encoder(data)]), fname);
}
