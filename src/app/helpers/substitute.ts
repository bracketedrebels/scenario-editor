export const substitute = <T>(list: T[]) => (index: number, fresh: T) => {
  return index < 0
   ? list
   : [
     ...list.slice(0, index),
     fresh,
     ...list.slice(index + 1)
   ];
}