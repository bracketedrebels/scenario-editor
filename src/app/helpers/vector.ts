import { distance as dstnc } from 'mathjs';


export const x = (v: [number, number]) => v[0];

export const y = (v: [number, number]) => v[1];

export const length = <T extends [number, number]>([x1, y1]: T, [x2, y2]: T) => Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
export const distance = (point: [number, number], line: [number, number, number]) => dstnc(point, line);
export const round = ([x, y]: [number, number]) => [Math.round(x), Math.round(y)] as [number, number];

export const positionByHelix = <T extends [number, number]>([[x1, y1], [x2, y2]]: [T, T], helix: number) => {
  switch (helix) {
    case 9: return [x1 + (x2 - x1) / 2, y1 + (y2 - y1) / 2] as T;
    case 8: return [x1, y1 + (y2 - y1) / 2] as T;
    case 7: return [x1, y2] as T;
    case 6: return [x1 + (x2 - x1) / 2, y2] as T;
    case 5: return [x2, y2] as T;
    case 4: return [x2, y1 + (y2 - y1) / 2] as T;
    case 3: return [x2, y1] as T;
    case 2: return [x1 + (x2 - x1) / 2, y1] as T;
    case 1:
    default:
  }
  return [x1, y1] as T;
}

export const angle = <T extends [number, number]>([x1, y1]: T, [x2, y2]: T) => Math.atan2(y2 - y1, x2 - x1);
export const add = <T extends [number, number]>([x1, y1]: T, [x2, y2]: T) => [ x1 + x2, y1 + y2 ] as T;
export const subtract = <T extends [number, number]>([x1, y1]: T, [x2, y2]: T) => [ x1 - x2, y1 - y2 ] as T;
