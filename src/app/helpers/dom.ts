import { Box } from '../types';

export const rect = (v: Element) => {
  const { top, right, bottom, left } = v.getBoundingClientRect();
  return [[left, top], [right, bottom]] as Box;
}