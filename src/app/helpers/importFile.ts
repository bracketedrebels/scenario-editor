import { fromEvent, Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';

const openHelper = document.createElement('input');
const readHelper = new FileReader();

openHelper.setAttribute('type', 'file');
openHelper.setAttribute('multiple', 'false');

export const importFile = <T>(decoder = (v: string) => (JSON.parse(v) as T)) => {
  openHelper.click();
  fromEvent(openHelper, 'change', { capture: true, passive: true })
    .pipe( first() )
    .subscribe(() => {
      readHelper.readAsText(openHelper.files[0]);
      openHelper.value = null;
    });

  return fromEvent(readHelper, 'load')
    .pipe(
      map(() => decoder(readHelper.result.toString())));
}