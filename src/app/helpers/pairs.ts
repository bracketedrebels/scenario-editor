export const pairs = <T>(list: T[]): [T, T][] => list
  .reduce((acc, v, i, list) => i === 0 ? acc : [...acc, [list[i - 1], v]], []);