import { Injectable } from "@angular/core";
import { CanActivate, UrlTree, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth';

@Injectable()
export class LoggedInGuard implements CanActivate {
  canActivate(): boolean | UrlTree {
    return this.authService.loggedin || this.router.createUrlTree(['/']);
  }

  constructor(
    private readonly authService: AuthenticationService,
    private readonly router: Router
  ) {}
}