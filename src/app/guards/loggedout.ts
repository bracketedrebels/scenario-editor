import { Injectable } from "@angular/core";
import { CanActivate, UrlTree, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../services/auth';

@Injectable()
export class LoggedOutGuard implements CanActivate {
  canActivate( router: ActivatedRouteSnapshot ): boolean | UrlTree {
    return !this.authService.loggedin || this.router.createUrlTree(['/']);
  }

  constructor(
    private readonly authService: AuthenticationService,
    private readonly router: Router
  ) {}
}