import { Component as NgComponent, ChangeDetectionStrategy } from '@angular/core';

@NgComponent({
  selector: 'brs-select',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Component { }
