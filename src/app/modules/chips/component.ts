import { Component as NgComponent, ChangeDetectionStrategy, Input, HostBinding } from "@angular/core";

@NgComponent({
  selector: 'brs-chips',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() @HostBinding('class.active') public readonly active = false;

  constructor() {
    console.log(encodeURI('#<>:'));
  }
}
