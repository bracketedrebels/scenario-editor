import { NgModule } from '@angular/core';
import { Component } from './component';

@NgModule({
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }