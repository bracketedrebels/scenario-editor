import { Component as NgComponent, Input, ChangeDetectionStrategy, Output, EventEmitter, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';

@NgComponent({
  selector: 'brs-input',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component implements AfterViewChecked {
  @Input() public readonly placeholder = '';
  @Input() public readonly value: string = '';
  @Input() public readonly caret: [number, number] = [0, 0];
  @Input() public readonly focus = false;

  @Output() public readonly value$ = new EventEmitter<string>();
  @Output() public readonly caret$ = new EventEmitter<[number, number]>();
  @Output() public readonly focus$ = new EventEmitter<boolean>();

  public ngAfterViewChecked(): void {
    this.silently = true;
    if (this.focus) this.context.focus();
    else this.context.blur();
    this.acknowlidgeCaretPosition();
    this.silently = false;
  }

  public oninput(e: Event): void {
    if (this.silently) return;
    const desired = this.context.value;
    if (this.value === desired) return;
    this.cancelEvent(e);
    this.processCaretChangeRequest();
    this.context.value = this.value;
    this.value$.emit(desired);
  }

  public onfocusin(e: FocusEvent): void {
    if (this.silently || this.focus) return;
    this.cancelEvent(e);
    this.focus$.emit(true);
  }

  public onfocusout(e: Event): void {
    if (this.silently || !this.focus) return;
    this.cancelEvent(e);
    this.focus$.emit(false);
  }

  public processCaretChangeRequest(): void {
    const cp = this.acquireCaretPosition();
    if (!this.caret || cp[0] !== (this.caret[0]) || cp[1] !== this.caret[1]) {
      this.acknowlidgeCaretPosition();
      this.caret$.emit(cp);
    }
  }



  @ViewChild('input') private readonly input: ElementRef;
  private silently = false;

  private cancelEvent(e: Event): void {
    e.preventDefault();
    e.stopImmediatePropagation();
  }

  private get context(): HTMLTextAreaElement { return this.input.nativeElement }

  private acknowlidgeCaretPosition(): void { this.context.setSelectionRange(this.caret && this.caret[0] || 0, this.caret && this.caret[1] || 0) }
  private acquireCaretPosition(): [number, number] { return [this.context.selectionStart, this.context.selectionEnd] }
}
