import { Component as NgComponent, ChangeDetectionStrategy, Input, Output, EventEmitter, Optional, Attribute, ContentChild, TemplateRef, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

@NgComponent({
  selector: 'brs-multivalue',
  templateUrl: 'component.html',
  styleUrls: [ 'component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('animated', [
      transition(':leave', [
        style({ width: '*', opacity: 1 }),
        animate('.2s ease-in-out', style({ width: 0, opacity: 0 }))])
    ])
  ]
})
export class Component<T = string> implements OnDestroy {
  @Input() public readonly value: T[] = [];
  @Input() public readonly emphasized: T | undefined = undefined;
  @Input() public readonly searching: string = '';
  @Input() public readonly focus: boolean = false;

  @Output() public readonly value$ = new EventEmitter<T[]>();
  @Output() public readonly emphasized$ = new EventEmitter<T | undefined>();
  @Output() public readonly searching$ = new EventEmitter<string>();
  @Output() public readonly focus$ = new EventEmitter<boolean>();

  @ContentChild(TemplateRef) public readonly view?: TemplateRef<T>;

  public tracker(_, v: T): T { return v }

  public shift(shift: number): false {
    const index = Math
      .min(this.value.length - 1, Math
        .max(0, this.emphasized
          ? this.value.findIndex(v => v === this.emphasized) + shift
          : shift < 0
            ? this.value.length + shift
            : shift));

    if (this.emphasized !== this.value[index]) {
      this.emphasized$.emit(this.value[index]); 
    }

    return false;
  }

  public remove(target: T): false {
    if (this.value.includes(target)) {
      this.value$
        .emit(this.value
          .filter(v => v !== target));
    }

    return false;
  }

  public atstart(cursor: [number, number]): boolean {
    return !cursor || (!cursor[0] && !cursor[1])
  }

  public atend(cursor: [number, number]): boolean {
    return this.searching
      ? cursor[0] === this.searching.length && cursor[1] === this.searching.length 
      : true;
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  constructor( @Optional() @Attribute('placeholder') public readonly placeholder: string ) { }



  private readonly subscription = this.searching$
    .subscribe(() => this.emphasized$.emit(undefined));
}
