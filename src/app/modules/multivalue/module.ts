import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { InputModule } from '../input';
import { ChipsModule } from '../chips';
import { NoLineBreaksPipeModule } from '../pipes/nolinebreaks';

@NgModule({
  imports: [ CommonModule, InputModule, NoLineBreaksPipeModule, ChipsModule ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
