import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map, distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { skipLineBreaks } from './helpers';

@NgPipe({
  name: 'nolinebreaks'
})
export class Pipe implements PipeTransform { 
  public transform(input: string | Observable<string>): string | Observable<string> {
    return input instanceof Observable
      ? input
          .pipe(
            map(v => v ? skipLineBreaks(v) : v),
            filter(v => !!v),
            distinctUntilChanged())
      : input && skipLineBreaks(input);
  }
}