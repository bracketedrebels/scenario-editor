export function skipLineBreaks(v: string): string {
  return v.replace(/[\r\n]/g, '');
}
