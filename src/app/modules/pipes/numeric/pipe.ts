import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith, combineLatest, filter, share } from 'rxjs/operators';

@NgPipe({
  name: 'numeric'
})
export class Pipe implements PipeTransform {
  public transform(obs: Observable<string>, fallback: number = 0): Observable<number> {
    return obs
      .pipe(
        map(v => Number(v || fallback)),
        combineLatest(obs
          .pipe(
            filter(v => !Number.isNaN(Number(v))),
            map(v => Number(v)),
            share())),
        map(([now, lastSuccess]) => Number.isNaN(now) ? lastSuccess : now),
        startWith(fallback));
  }
}