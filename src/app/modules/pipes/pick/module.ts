import { NgModule } from '@angular/core';
import { Pipe } from './pick.pipe';

@NgModule({
  declarations: [ Pipe ],
  exports: [ Pipe ]
})
export class Module { }
