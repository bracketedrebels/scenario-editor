import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Dictionary } from 'lodash';

@NgPipe({
  name: 'pick'
})
export class Pipe implements PipeTransform {
  public transform<T, U extends (Dictionary<T> | Array<T>)>(value: U | Observable<U>, key: keyof U): U[typeof key] | Observable<U[typeof key]> {
    return value instanceof Observable
      ? value.pipe(map(v => v[key]))
      : value[key];
  }
}
