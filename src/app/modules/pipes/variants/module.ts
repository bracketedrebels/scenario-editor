import { NgModule } from '@angular/core';
import { Pipe } from './pipe';

@NgModule({
  declarations: [ Pipe ],
  exports: [ Pipe ]
})
export class Module { }
