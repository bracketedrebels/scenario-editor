import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@NgPipe({ name: 'variants' })
export class Pipe implements PipeTransform {
  public transform(obs: Observable<string>, variants: string[]): Observable<string> {
    return obs
      .pipe(
        map(v => new RegExp(['.*', v.split('').join('.*'), '.*'].join(''))),
        map(regex => variants.find(v => regex.test(v)) || ''));
  }
}