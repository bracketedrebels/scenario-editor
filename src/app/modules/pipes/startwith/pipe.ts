import { PipeTransform, Pipe as NgPipe } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@NgPipe({
  name: 'startwith'
})
export class Pipe implements PipeTransform {
  public transform<T>(value: Observable<T>, init: T): Observable<T> {
    return value.pipe( startWith(init) )
  }
}