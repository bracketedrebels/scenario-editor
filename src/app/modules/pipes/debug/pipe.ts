import { PipeTransform, Pipe as NgPipe } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@NgPipe({
  name: 'debug'
})
export class Pipe implements PipeTransform {
  public transform(value: Observable<any> | any, message?: string): typeof value {
    return value instanceof Observable
      ? value.pipe( tap(v => console.log(message || v)) )
      : (console.log(message || value), value);
  }
}
