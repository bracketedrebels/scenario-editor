import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map, combineLatest, filter, share } from 'rxjs/operators';

@NgPipe({
  name: 'pattern'
})
export class Pipe implements PipeTransform {
  public transform(obs: Observable<string>, regexp: string): Observable<string> {
    const regex = new RegExp(regexp, 'm');
    return obs
      .pipe(
        combineLatest(obs
          .pipe(
            filter(v => regex.test(v)),
            share())),
        map(([now, lastSuccess]) => regex.test(now) ? now : lastSuccess));
  }
}