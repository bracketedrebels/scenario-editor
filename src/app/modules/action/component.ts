import { Component as NgComponent, ChangeDetectionStrategy, Input, Output, EventEmitter, SimpleChanges, ElementRef, HostListener, HostBinding } from "@angular/core";

@NgComponent({
  selector: 'brs-action',
  templateUrl: 'component.html',
  styleUrls: ['component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component {
  @Input() public readonly focused: boolean = false;
  @Input() public readonly disabled = false;
  @Input() public readonly icon: string;

  @Output() public readonly activated$ = new EventEmitter<void>();
  @Output() public readonly focused$ = new EventEmitter<boolean>();

  public ngOnChanges({focused, disabled}: SimpleChanges): void {
    const native: HTMLElement = this.ref.nativeElement;
    if (focused) {
      if (focused.currentValue) {
        native.focus();
      } else {
        native.blur();
      }
    }

    if (disabled) {
      if (disabled.currentValue) {
        this.tabindex = null;
      } else {
        this.tabindex = 1;
      }
    }
  }

  constructor ( private readonly ref: ElementRef ) {}



  @HostBinding('attr.tabindex') private tabindex: 1 | null = 1;

  @HostListener('keyup.enter')
  @HostListener('keyup.space')
  @HostListener('click')
  private onActivated(): void {
    this.activated$.emit();
  }

  @HostListener('focusin')
  private requestFocus(): void {
    this.focused$.emit(true);
  }

  @HostListener('focusout')
  private requestBlur(): void {
    this.focused$.emit(false);
  }
}
