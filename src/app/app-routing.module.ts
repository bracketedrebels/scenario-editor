import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [RouterModule.forRoot([{
    path: 'editor',
    loadChildren: './+editor/module#Module'
  }, {
    path: '',
    redirectTo: 'editor',
    pathMatch: 'full'
  }])],

  exports: [RouterModule]
})
export class AppRoutingModule { }
